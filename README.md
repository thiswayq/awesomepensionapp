# Webapp log parser

The app will receive a .log file and order the entries into most viewed and also unique views.
I decided to make some improvements that was to use the output to a CSV file as it's better for human eyes than a cli.
Another improvement was to upload to s3, it was something simple and I think it would demonstrate my skills and experience with aws.
As this would require to set env vars for the usage I decided to make it a small sinatra app, where it would be better to input the file.
I also decided to name the files with a timestamp for the case of the final user want to access older ones, or if in the future we decided to add a history of the uploads and still leave the files available to re-download.

# Requirements

### Required gems

- Bundler

and then

> bundle install

For tests:

- RSpec

# How to run

To run locally you need to set the s3 envs in .env file, I can provide anytime.
But for a smooth proccess I also deployed the application: https://awesomepensionapp.herokuapp.com/

Accessing the site you should upload the webserver.log file and it will be uploaded on s3.
The server will deliver the downloadable links of both files as response.

# Tests

You can just run:

> rspec.

Ps: I didn't have enough time to test the sinatra methods.

# Possible issues

The application is not covering the situation of a .log file with different format from multiple lines with 'site ip'.
There's also no validator about the ip.
