# frozen_string_literal: true
require 's3_client'

describe S3Client do
  describe '#call' do
    subject(:s3) { described_class.new }

    context 'when using the s3 client' do
      it 'should upload both metrics files on s3' do
        allow_any_instance_of(Aws::S3::Client).to receive(:new).and_return(true)
        allow_any_instance_of(Aws::S3::Client).to receive(:put_object).and_return(true)
        expect(s3.call).to start_with("Files uploaded:")
      end
    end

    context 'when using the s3 client and finds an error' do
      it 'should raise error' do
        allow_any_instance_of(Aws::S3::Client).to receive(:new).and_return(true)
        allow_any_instance_of(Aws::S3::Client).to receive(:put_object).and_raise("Error")
        expect(s3.call).to eq("Error uploading file Error")
      end
    end
  end
end