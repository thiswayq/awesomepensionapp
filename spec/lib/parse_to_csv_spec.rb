# frozen_string_literal: true
require 'parse_to_csv'

describe ParseToCSV do
  describe '#call' do
    subject(:parser) { described_class.new("most_viewed", views) }
    let(:views) { { '/home' => 4, '/about' => 2 } }
    let(:generated_csv) { File.read("./files/most_viewed.csv") }

    context 'when parsing the views' do
      it 'should create the csv file with the metricss' do
        expect(parser.call).to eq(views)
        expect(generated_csv).to eq("Site,Views\n/home,4\n/about,2\n")
      end
    end

    context 'when parsing the views but views are empty' do
      let(:views) { {} }

      it 'should create the csv file with empty metrics' do
        expect(parser.call).to eq(views)
        expect(generated_csv).to eq("Site,Views\n")
      end
    end
  end
end

