# frozen_string_literal: true
require 'views_counter'

describe ViewsCounter do
  describe '#call' do
    subject(:counter) { described_class.new(log) }
    let(:log) { File.read("./spec/fixture/webserver_fixture.log") }
    let(:most_viewed_csv) { File.read("./files/most_viewed.csv") }
    let(:unique_views_csv) { File.read("./files/unique_views.csv") }

    context 'when counting the views with log file' do
      it 'should create the csv file for both unique and most views ordered' do
        counter.call
        expect(most_viewed_csv).to eq("Site,Views\n/contact,2\n/home,2\n/help_page/1,1\n")
        expect(unique_views_csv).to eq("Site,Views\n/contact,2\n/home,1\n/help_page/1,1\n")
      end
    end

    context 'when counting the views with empty log file' do
      let(:log) { File.read("./spec/fixture/empty_webserver_fixture.log") }

      it 'should create the csv file with empty lines' do
        counter.call
        expect(most_viewed_csv).to eq("Site,Views\n")
        expect(unique_views_csv).to eq("Site,Views\n")
      end
    end
  end
end

