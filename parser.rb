require_relative './lib/views_counter'
require_relative './lib/s3_client'
require 'dotenv'
require 'sinatra/base'

class Parser < Sinatra::Base
  Dotenv.load if ENV['RACK_ENV'] == 'development'

  get '/' do
    erb :upload
  end

  post '/upload' do
    tempfile = params[:file][:tempfile]

    raise "Wrong file format" if File.extname(tempfile) != ".log"

    ViewsCounter.new(tempfile).call
    S3Client.new.call
  end
end