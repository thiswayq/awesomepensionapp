# frozen_string_literal: true
require_relative 'parse_to_csv'

class ViewsCounter
  def initialize(file)
    @file = file
    @most_views = Hash.new(0)
    @unique_views = Hash.new(0)
  end

  def call
    calculate_views
    parse_files("most_viewed", @most_views)
    parse_files("unique_views", @unique_views)
  end

  private

  def calculate_views
    unique_access = []

    @file.each_line do |line|
      site = line.split[0]
      @most_views[site] += 1
      @unique_views[site] += 1 if !unique_access.include?(line)
      unique_access << line
    end

    @most_views = @most_views.sort_by {|site, views| views}.reverse
    @unique_views = @unique_views.sort_by {|site, views| views}.reverse
  end

  def parse_files(type, views)
    ParseToCSV.new(type, views).call
  end
end