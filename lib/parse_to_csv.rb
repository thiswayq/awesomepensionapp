# frozen_string_literal: true
require 'csv'

class ParseToCSV
  def initialize(type, views)
    @file_path = "./files/#{type}.csv"
    @views = views
  end

  def call
    create_csv
  end

  private

  def create_csv
    headers = ["Site", "Views"]

    CSV.open(@file_path, "wb", write_headers: true, headers: headers ) do |csv|
      @views.each do |row|
        csv << row
      end
    end
  end
end