# frozen_string_literal: true
require 'aws-sdk-s3'
require 'dotenv'
Dotenv.load

class S3Client

  def initialize
    @s3 = Aws::S3::Client.new
  end

  def call
    upload_to_s3
  end

  private

  def upload_to_s3
    files = {
      most_viewed: "most_viewed_#{Time.now.to_i}.csv",
      unique_views: "unique_views_#{Time.now.to_i}.csv"
    }

    files.each do |path_name, file_name|
      @s3.put_object(
        bucket: "simplepension",
        key: file_name,
        content_disposition: "attachment",
        body: File.open("./files/#{path_name}.csv")
      )
    end

    success_message(files)
  rescue => e
    "Error uploading file #{e}"
  end

  def success_message(files)
    message = "Files uploaded: "
    message +="<a href='#{get_s3_link(files[:most_viewed])}'>Most viewed</a> and "
    message += "<a href='#{get_s3_link(files[:unique_views])}'>Unique views</a>"
  end

  def get_s3_link(file_name)
    "https://simplepension.s3.us-east-2.amazonaws.com/#{file_name}"
  end
end